package routes

import (
	"github.com/gin-gonic/gin"
	"gitee.com/goroseGin/bootstrap"
	"net/http"
	"time"
)

func AdminRun() {
	var router gin.IRouter = bootstrap.GetBooterInstance().Router

	router.GET("/admin", func(c *gin.Context) {
		format := "2006-01-02 15:04:05"
		//go func() {
		//	time.Sleep(3*time.Second)
		//	log.Println(time.Now().Format(format))
		//}()
		c.String(http.StatusOK, "Admin works "+time.Now().Format(format))
	})


}